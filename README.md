# myMultipotentialiteInterestsViz

Multipotentialite visualisation of my own interests.

![Image of Visualisation](./myMultipotentialiteInterestsViz.png)

This is an [amcharts force directed tree](https://www.amcharts.com/docs/v4/chart-types/force-directed/) visualisation of (some) of my interests and hobbies over the years. 

## Installation

Download the code and open index.html in your browser while you have an internet connection. It should work straight away from most broweser except chrome.

Note:Due to security limitations in Chrime if you want to run your own local version of this and view it in your Chrome browser you will need to download the code and run it from your local webserver (i.e. localhost). 

## Running a version for your own interests
If you want to do this with your own data you simply need to 

1) Firstly ensure that you can get the diagram working with the existing mydata.json file

2) Once that is done you replace the content of the mydata.json file with valid json listing YOUR interests in the same format.

3) If you have an error in your json data the diagram simply won't display. I recommend that if this happens you use the following [JSON Validator](https://jsonformatter.curiousconcept.com/) to check and correct the format of your list of interests.

*A note on sizing: In my data I used the "value" field to indicate a level of experience. In most cases I set the "value" of each interest to a number representing 1-5 years (yes there are exceptions in some cases). If you are not already familiar with force directed charts please be aware the "value" field directly impacts the size of the displayed nodes.*

## Demo
To see the live demo of this visualisation working please visit [my blog](https://sineadobrien.com/tech/multipotentialite-visualisation-of-interests/)

## License
[![License: CC BY-NC-SA 4.0](https://licensebuttons.net/l/by-nc-sa/4.0/80x15.png)](https://creativecommons.org/licenses/by-nc-sa/4.0/)